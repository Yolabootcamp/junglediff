import { Card } from 'react-bootstrap'
import React from 'react'
// import Champie from "../../assets/Champie.png";
// import axios from 'axios';


interface ChampionImage {
    full: string,
    sprite: string,
    group: string,
    x: number,
    y: number,
    w: number,
    h: number
}

interface Champion {
    blurb: string,
    id: string,
    image: ChampionImage,
    info: {},
    key: string,
    partype: string,
    stats: {},
    tags: {},
    title: string,
    version: string
}



interface IChampionCardProps{
    champion: Champion
}


export default class ChampionCard extends React.Component<IChampionCardProps> {
    
    championId: string = this.props.champion.id;
    


    render() {

        return (
            <Card style={{ width: '100px', height: '150px', backgroundColor: 'transparent', border: '0px'}}>
                <Card.Img variant="top" style={{width: '100px'}} src={`https://ddragon.leagueoflegends.com/cdn/11.20.1/img/champion/${this.championId}.png`} />
                <Card.Body style={{display: 'flex', justifyContent: 'center', width: '70px'}}>
                    <Card.Text style={{ fontSize: '20px', fontWeight: 500, color:'#e6e0cf', width: '100px'}}>{this.championId}
                    </Card.Text>
                </Card.Body>
            </Card>
        )
    }
}

