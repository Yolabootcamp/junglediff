import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
// import "./championsPage.css";

import "./left-container.css";
import "./right-container.css";
import LeftContainer from "./LeftContainer";
import RightContainer from "./RightContainer";

export default class Champions extends React.Component {
  render() {
    return (
      <Router>
        <div className="champions-page">
          <div className="cp-container">

            <div className="cp-left-container">
                <LeftContainer />
            </div>

            <div className="cp-right-container">
              <RightContainer />
            </div>
            
          </div>
        </div>
      </Router>
    );
  }
}
